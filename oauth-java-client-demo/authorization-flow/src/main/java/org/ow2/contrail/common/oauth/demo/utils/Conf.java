package org.ow2.contrail.common.oauth.demo.utils;

import org.apache.log4j.Logger;
import org.ow2.contrail.common.oauth.client.TokenValidationProtocol;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/***
 * Configuration class to initialise the OAuth AS and security settings
 * 
 * @author a.memon
 */
public class Conf {
	private static Conf instance = new Conf();
	private static Logger log = Logger.getLogger(Conf.class);
	private Properties props;

	public static Conf getInstance() {
		return instance;
	}

	private Conf() {
	}

	public void load(File confFile) throws IOException {
		log.info(String.format("Loading configuration from file '%s'.",
				confFile.getAbsolutePath()));
		this.props = new Properties();
		try {
			this.props.load(new FileInputStream(confFile));
			log.info("Configuration loaded successfully.");
		} catch (IOException e) {
			throw new IOException(String.format(
					"Failed to read configuration file '%s'.",
					confFile.getAbsolutePath()));
		}
	}

	public String getClientId() {
		String clientId = props.getProperty("client.id");
		if (clientId == null) {
			log.error("OAuth2 Client Id MUST NOT be null. Register with an OAuth2 AS server to get one");
		}
		return clientId;
	}

	public String getClientSecret() {
		String clientSecret = props.getProperty("client.secret");
		if (clientSecret == null) {
			log.error("OAuth2 Client secret MUST NOT be null. Register with an OAuth2 AS server to get one");
		}
		return clientSecret;
	}

	public String getClientOauth2CallbackUri() {
		String callbackUri = props.getProperty("client.oauth2callbackUri");
		if (callbackUri == null) {
			log.error("OAuth2 callback URI - an address to receive the access tokens - is MANDATORY and MUST NOT be null");
		}
		return callbackUri;
	}

	public String getClientKeystoreFile() {
		return props.getProperty("client.keystore.file");
	}

	public String getClientKeystorePass() {
		return props.getProperty("client.keystore.pass");
	}

	public String getClientKeystoreType() {
		String keystoreType = props.getProperty("client.keystore.type");
		if (keystoreType == null) {
			log.info("Keystore type is NULL, default value is set to 'JKS'");
			keystoreType = "JKS";
		}
		return keystoreType;
	}

	public String getClientKeystoreAlias() {
		return props.getProperty("client.keystore.alias");
	}

	public String getClientTruststoreFile() {
		return props.getProperty("client.truststore.file");
	}

	public String getClientTruststorePass() {
		return props.getProperty("client.truststore.pass");
	}

	public String getASAuthorizationUri() {
		String oauthAsUri = props
				.getProperty("authzserver.authorizationEndpointUri");
		if (oauthAsUri == null) {
			log.error("OAuth2 Authorisation Server (AS) AuthZ Endpoint URI is MANDATORY and MUST NOT be null");
		}
		return oauthAsUri;
	}

	public String getASAccessTokenUri() {
		String tokenUri = props
				.getProperty("authzserver.accessTokenEndpointUri");
		if (tokenUri == null) {
			log.error("OAuth2 Authorisation Server (AS) Token Endpoint URI is MANDATORY and MUST NOT be null");
		}
		return tokenUri;
	}

	public String getASAccessTokenValidationUri() {
		String tokenValUri = props
				.getProperty("authzserver.accessTokenValidationEndpointUri");
		if (tokenValUri == null) {
			log.error("OAuth2 Authorisation Server (AS) Token Validation Endpoint URI is MANDATORY and MUST NOT be null");
		}
		return tokenValUri;
	}

	public String getCAUserCertUri() {
		return props.getProperty("caserver.userCertUri");
	}

	public String getScope() {
		return props.getProperty("scope");
	}

	public TokenValidationProtocol getAccessTokenValidationProtocol() {
		return TokenValidationProtocol.fromString(props
				.getProperty("authzserver.accessTokenValidationProtocol"));
	}
}