package org.ow2.contrail.common.oauth.demo.utils;

import org.apache.log4j.Logger;
import org.bouncycastle.openssl.PEMWriter;
import org.ow2.contrail.common.oauth.client.CertRetriever;
import org.ow2.contrail.common.oauth.client.KeyAndCertificate;

import eu.emi.security.authn.x509.impl.KeyAndCertCredential;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyStore;

/***
 * Utility class to fetch a certificate from online CA
 * 
 * @author a.memon
 */
public class CertUtils {
	private static Logger log = Logger.getLogger(CertUtils.class);
	/**
	 * 
	 * 
	 * @param accessToken
	 * @return {@link KeyAndCertificate} object encapsulating public and private
	 *         keypairs
	 * @throws Exception
	 */
	public static KeyAndCertificate retrieveCert(String accessToken) throws Exception {
		KeyAndCertificate kc = getCertretriever().retrieveCert(accessToken);
		log.info("Key and certificate credentials: "+kc.toString());
		return kc;
	}
	
	public static String retrieveProxyCert(String accessToken) throws Exception {
		return getCertretriever().retrieveProxyCert(accessToken);
	}
	
	static CertRetriever getCertretriever(){
		URI endpointUri = null;
		try {
			endpointUri = new URI(Conf.getInstance().getCAUserCertUri());
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		CertRetriever certRetriever = new CertRetriever(endpointUri, Conf.getInstance().getClientKeystoreFile(),
				Conf.getInstance().getClientKeystorePass(), Conf.getInstance().getClientKeystoreType(),
				Conf.getInstance().getClientTruststoreFile(), Conf.getInstance().getClientTruststorePass(),
				Conf.getInstance().getClientId(), Conf.getInstance().getClientSecret());
		return certRetriever;
	}

	/***
	 * Converts incoming serialised object from online CA to string PEM format
	 * 
	 * @param o
	 *            serialised object
	 * @return String PEM
	 * @throws IOException
	 */
	public static String convertToPem(Object o) throws IOException {
		StringWriter writer = new StringWriter();
		PEMWriter pemWriter = new PEMWriter(writer);
		pemWriter.writeObject(o);
		pemWriter.flush();
		pemWriter.close();
		return writer.toString();
	}

}
