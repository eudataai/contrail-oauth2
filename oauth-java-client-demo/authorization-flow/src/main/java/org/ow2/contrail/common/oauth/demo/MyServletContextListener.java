package org.ow2.contrail.common.oauth.demo;

import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.ow2.contrail.common.oauth.demo.utils.Conf;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;
import java.security.Security;

public class MyServletContextListener implements ServletContextListener {
	private static Logger log = Logger.getLogger(Conf.class);

	static{
		Security.addProvider(new BouncyCastleProvider());
	}
	
	public MyServletContextListener() {

	}

	public void contextInitialized(ServletContextEvent contextEvent) {
		log.info("OAuth Certificate Client version: "+MyServletContextListener.class.getPackage()
				.getImplementationVersion());
		try {

			ServletContext context = contextEvent.getServletContext();
			String configFilePath = context
					.getInitParameter("configuration-file");
			if (configFilePath == null) {
				throw new Exception(
						"Missing setting 'configuration-file' in web.xml.");
			}

			Conf.getInstance().load(new File(configFilePath));

			log.debug("Client-ID: " + Conf.getInstance().getClientId());
			log.debug("Access Token URI: "
					+ Conf.getInstance().getASAccessTokenUri());
			log.debug("Access Token Validation URI: "
					+ Conf.getInstance().getASAccessTokenValidationUri());
			log.debug("Authorisation Server URI: "
					+ Conf.getInstance().getASAuthorizationUri());
			log.debug("OAuth Callback URI: "
					+ Conf.getInstance().getClientOauth2CallbackUri());
			log.debug("Address of online CA server (Resource Server): "
					+ Conf.getInstance().getCAUserCertUri());

		} catch (Exception e) {
			log.error("OAuth-java-client-demo webapp failed to start: "
					+ e.getMessage());
			throw new RuntimeException(
					"OAuth-java-client-demo webapp failed to start: "
							+ e.getMessage());
		}

		log.info("OAuth-java-client-demo was initialized successfully.");
	}

	public void contextDestroyed(ServletContextEvent contextEvent) {
	}
}