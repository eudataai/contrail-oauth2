package org.ow2.contrail.common.oauth.demo.utils;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.ow2.contrail.common.oauth.client.ITokenInfo;
import org.ow2.contrail.common.oauth.client.TokenInfo;
import org.ow2.contrail.common.oauth.client.TokenValidationProtocol;
import org.ow2.contrail.common.oauth.client.TokenValidator;
import org.ow2.contrail.common.oauth.rp.TokenValidatorFactory;

import eu.unicore.security.canl.AuthnAndTrustProperties;
import eu.unicore.security.canl.CredentialProperties;
import eu.unicore.security.canl.DefaultAuthnAndTrustConfiguration;
import eu.unicore.security.canl.TrustedIssuersProperties.TruststoreType;
import eu.unicore.security.canl.TruststoreProperties;
import eu.unicore.util.Log;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class TokenUtils {
	private static Logger log = Log.getLogger("OAuth-Client", TokenUtils.class);
	public static String getAuthorizationRequestUri(String state) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("response_type", "code"));
		params.add(new BasicNameValuePair("redirect_uri", Conf.getInstance()
				.getClientOauth2CallbackUri()));
		params.add(new BasicNameValuePair("client_id", Conf.getInstance()
				.getClientId()));
		params.add(new BasicNameValuePair("state", state));
		params.add(new BasicNameValuePair("scope", Conf.getInstance()
				.getScope()));

		String query = URLEncodedUtils.format(params, "utf-8");
		return Conf.getInstance().getASAuthorizationUri() + "?" + query;
	}

	public static TokenInfo _getTokenInfo(String accessToken) throws Exception {
		String u = Conf.getInstance().getASAccessTokenValidationUri();
		URI endpointUri = new URI(Conf.getInstance()
				.getASAccessTokenValidationUri());
		TokenValidator tokenValidator = null;
		TokenInfo info = null;
		try {

			if (u.startsWith("https")) {
				tokenValidator = new TokenValidator(endpointUri, Conf
						.getInstance().getClientKeystoreFile(), Conf
						.getInstance().getClientKeystorePass(), Conf
						.getInstance().getClientKeystoreType(), Conf
						.getInstance().getClientTruststoreFile(), Conf
						.getInstance().getClientTruststorePass());
				// info = tokenValidator.checkToken(accessToken,
				// "oauth-java-client-demo");
				info = tokenValidator.checkToken(accessToken);
			} else {
				tokenValidator = new TokenValidator(endpointUri);
				info = tokenValidator.checkTokenHttp(accessToken,
						"oauth-java-client-demo");
			}

		} catch (Exception e) {
			throw new Exception(e);
		}
		return info;
	}
	
	/**
	 * Validates and fetches access token meta information
	 * 
	 * @param accessToken
	 * @return
	 * @throws Exception
	 */
	public static ITokenInfo getTokenInfo(String accessToken) throws Exception {
		String u = Conf.getInstance().getASAccessTokenValidationUri();
		
		URI endpointUri = new URI(Conf.getInstance()
				.getASAccessTokenValidationUri());
		TokenValidator tokenValidator = null;
		ITokenInfo info = null;
		
		TokenValidationProtocol protocol = Conf.getInstance().getAccessTokenValidationProtocol();
		System.out.println("validation endpoint is: "+u);
		org.ow2.contrail.common.oauth.rp.TokenValidator validator = TokenValidatorFactory.newInstance(protocol, u, getAuthnAndTrustProperties());
		try {
			
			if (u.startsWith("https")) {
				info = validator.verifyToken(accessToken);
			} else {
				tokenValidator = new TokenValidator(endpointUri);
				info = tokenValidator.checkTokenHttp(accessToken,
						"oauth-java-client-demo");
			}

		} catch (Exception e) {
			throw new Exception(e);
		}
		return info;
	}
	
	public static AuthnAndTrustProperties getAuthnAndTrustProperties() {
		Properties p = new Properties();
		// Credential Properties
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_FORMAT,
				Conf.getInstance().getClientKeystoreType());
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_KEY_LOCATION, Conf
						.getInstance().getClientKeystoreFile());
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_LOCATION, Conf
				.getInstance().getClientKeystoreFile());
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_KS_KEY_PASSWORD, Conf
				.getInstance().getClientKeystorePass());
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_PASSWORD, Conf
				.getInstance().getClientKeystorePass());

		// truststore properties
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_TYPE,
				TruststoreType.keystore.toString());
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_TYPE,
				"jks");
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_PATH,
				Conf.getInstance().getClientTruststoreFile());
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_PASSWORD,
				Conf.getInstance().getClientTruststorePass());
			
		AuthnAndTrustProperties auth = new AuthnAndTrustProperties(p);
		
		return auth;
	}
}
