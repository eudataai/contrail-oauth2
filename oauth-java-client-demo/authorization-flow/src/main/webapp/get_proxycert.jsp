<%@page import="xmlbeans.org.oasis.saml2.assertion.AssertionDocument"%>
<%@page import="java.util.Arrays"%>
<%@page import="eu.emi.security.authn.x509.helpers.proxy.ProxySAMLExtension"%>
<%@page import="eu.emi.security.authn.x509.impl.CertificateUtils"%>
<%@ page import="org.ow2.contrail.common.oauth.client.KeyAndCertificate" %>
<%@ page import="org.ow2.contrail.common.oauth.demo.utils.CertUtils" %>
<%@ page import="org.ow2.contrail.common.oauth.demo.utils.Conf" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="java.io.OutputStream" %>
<%@ page import="java.io.*" %>
<%@ page import="java.io.BufferedOutputStream" %>
<%@ page import="java.io.FileWriter" %>
<%@ page import="java.io.File" %>
<%@ page import="java.security.KeyStore" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String accessToken = (String) session.getAttribute("access_token");
    if (accessToken == null) {
        response.sendRedirect("get_token.jsp");
    }

    String keyAndCertificate = CertUtils.retrieveProxyCert(accessToken);
    
    ByteArrayInputStream bis = new ByteArrayInputStream(keyAndCertificate.getBytes());
	    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>OAuth-Java-Client-Demo</title>
</head>
<body>
<h1>EUDAT OAuth 2.0 and Certificate Client</h1>

<p>
    <b>User certificate fetched from the CA server:</b> <%= Conf.getInstance().getCAUserCertUri() %>
    <br/>
    <b>Access token used:</b> <%= accessToken %>
    <%
      session.setAttribute("pkpair", keyAndCertificate.trim());
      ByteArrayOutputStream os = new ByteArrayOutputStream();
      %>
        
</p>
<br />
<FONT COLOR="green" size="5">SUCCESS: The certificate has been retrieved!!!</FONT><br/><br/>
<h2>
Download your short-lived X509 Credential as:
</h2>
<p>
<ul>
	<li>a <a href="download_cert.jsp">PEM file</a></li>
</ul>
</p>

<h2>
Certificate Information
</h2>
<p>
    <b>X509 Proxy Credential</b>
    <pre>
    <textarea rows="40" style="width:50%; margin:0; padding:15px 15px 15px;font:13px Tahoma, cursive;display:block;max-width:100%;line-height:1.5;" readonly="readonly">
    <%=keyAndCertificate %>
    </textarea>
    </pre>
	<br/>
</p>


<a href="get_token.jsp">Back</a>
</body>
</html>
