<%@page import="org.ow2.contrail.common.oauth.demo.utils.Conf"%>
<%@page import="org.json.JSONObject"%>
<%@ page import="org.ow2.contrail.common.oauth.client.ITokenInfo" %>
<%@ page import="org.ow2.contrail.common.oauth.demo.utils.TokenUtils" %>
<%@ page import="java.util.UUID" %>
<%
    String accessToken = (String) session.getAttribute("access_token");
    String scope = (String) session.getAttribute("scope");
    String clientId = (String) session.getAttribute("client_id");
    String tokenInfo = null;
    if (accessToken != null) {
        try {
        	// TokenUtils.getTokenInfo(accessToken);
        	if (!clientId.startsWith("myproxy")){
        		tokenInfo = TokenUtils.getTokenInfo(accessToken).toString();	
        	}
            
        }
        catch (Exception e) {
            throw new Exception("Failed to get access token information: " + e.getMessage());
        }
    }

    String state = UUID.randomUUID().toString();
    String authorizationRequestUri = TokenUtils.getAuthorizationRequestUri(state);
    session.setAttribute("state", state);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Simple OAuth and Certificate Client</title>
</head>
<body>
<h1>EUDAT Simple OAuth and Certificate Client</h1>
<%
    if (request.getParameter("error") != null) {
%>
<p style="color:red;"><%= request.getParameter("error") %>
</p>
<%
    }
%>

<%
    if (request.getParameter("message") != null) {
%>
<p style="color:green;"><%= request.getParameter("message") %>
</p>
<%
    }
%>

<strong>Client ID: </strong>
<%= Conf.getInstance().getClientId()%>
<br/>
<strong>Access token:</strong>
<%= accessToken != null ? accessToken : "Not available" %>
<br/>

<strong>Token information:</strong>
<pre><%= tokenInfo != null ? tokenInfo : "" %></pre>
<% 
	JSONObject jsonContent = null;
	String cont = null;
	if ((session.getAttribute("content") != null) && (tokenInfo == null)){
		jsonContent = new JSONObject((String)session.getAttribute("content"));
		cont = jsonContent.toString(3);
		out.write(cont);
	}
%>


<p>
    <strong>Request a new access token:</strong>
    <br/>
    By clicking the link you will be redirected to the Authorization Server.
    <br/>
    <a href="<%= authorizationRequestUri %>">Request an access token</a>
</p>
<% if ((session.getAttribute("content") != null)){ %>
<h3>Access protected resources</h3>
<%
    if ((accessToken != null) && (!clientId.contains("myproxy"))) {
%>

<a href="get_cert.jsp">Request user certificate</a>
<%
    } else {
%>
<a href="get_proxycert.jsp">Request user certificate</a>
<%
    }
}
%>
</body>
</html>
