<%@page import="java.nio.charset.StandardCharsets"%>
<%@ page import="org.ow2.contrail.common.oauth.client.KeyAndCertificate" %>
<%@ page import="org.ow2.contrail.common.oauth.demo.utils.CertUtils" %>
<%@ page import="org.ow2.contrail.common.oauth.demo.utils.Conf" %>
<%@ page import="java.io.FileOutputStream" %>
<%@ page import="java.io.OutputStream" %>
<%@ page import="java.io.*" %>
<%@ page import="java.io.BufferedOutputStream" %>
<%@ page import="java.io.FileWriter" %>
<%@ page import="java.io.File" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String accessToken = (String) session.getAttribute("access_token");
    if (accessToken == null) {
        response.sendRedirect("get_token.jsp");
    }

    
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>



    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>OAuth-Java-Client-Demo</title>
</head>
<body>
<h1>Contrail OAuth 2.0 Certificate Client</h1>

<p>
    
    <br/>
    Using access token: <%= accessToken %>
    <%
    
	  response.setContentType("application/octet-stream");
  	  response.setHeader("Content-Disposition",
      	    "attachment;filename=downloadme.pem" );
  	String pk_pub = (String)session.getAttribute("pkpair");
    InputStream is = new ByteArrayInputStream(pk_pub.getBytes(StandardCharsets.UTF_8));
    ServletOutputStream os = response.getOutputStream();
      
      
      int read=0;
      byte[] bytes = new byte[pk_pub.getBytes().length];
      
      while((read = is.read(bytes))!= -1){
          os.write(bytes, 0, read);
      }
      os.flush();
      os.close(); 
      
       
             
      
       %>
        
</p>
<br />

<a href="get_cert.jsp">Back</a>
</body>
</html>
