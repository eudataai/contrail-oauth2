package org.ow2.contrail.common.oauth.demo;

import java.net.URL;

import org.eclipse.jetty.server.AbstractConnector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.WebAppContext;

import eu.unicore.util.configuration.ConfigurationException;
import eu.unicore.util.jetty.SecuredServerConnector;

public class OAuthClientDemoJettyServer {
	private Server jetty;
	public final static String baseUrl = "https://localhost:8082";
	public void start(String appName) {
		try {
			jetty = new Server();
			WebAppContext context = new WebAppContext();
			context.setContextPath("/" + appName);
			context.setWar("src/main/webapp");
			context.setServer(jetty);
			
			jetty.setHandler(context);
			jetty.addConnector(createSecureConnector(new URL(baseUrl)));
			jetty.start();
			jetty.join();
			jetty.setStopAtShutdown(true);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void stop() {
		try {
			jetty.stop();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		OAuthClientDemoJettyServer j = new OAuthClientDemoJettyServer();
		j.start("oauth-authorization-flow-demo");
	}

	protected AbstractConnector createSecureConnector(URL url)
			throws ConfigurationException {
		boolean useNio = true;
		SecuredServerConnector ssl;
		
		System.out.println("Creating SSL NIO connector on: " + url);
		ssl = getNioSecuredConnectorInstance(url);
		SslContextFactory factory = ssl.getSslContextFactory();
		factory.setNeedClientAuth(false);
//		String disabledCiphers = extraSettings
//				.getValue(HttpServerProperties.DISABLED_CIPHER_SUITES);
//		if (disabledCiphers != null) {
//			disabledCiphers = disabledCiphers.trim();
//			if (disabledCiphers.length() > 1)
//				factory.setExcludeCipherSuites(disabledCiphers.split("[ ]+"));
//		}
		System.out.println("SSL protocol was set to: '" + factory.getProtocol() + "'");
		return (AbstractConnector) ssl;
	}

	/**
	 * @return an instance of NIO secure connector. It uses proper validators
	 *         and credentials and lowResourcesConnections are set to the
	 *         difference between MAX and LOW THREADS.
	 */
	protected SecuredServerConnector getNioSecuredConnectorInstance(URL url) {
		SecuredServerConnector ssl;
		SslContextFactory secureContextFactory;
		try {
			
			secureContextFactory = SecuredServerConnector.createContextFactory(
					OAuthClientDemoSecurityConfiguration.getAuthnAndTrustProperties().getValidator(), 
					OAuthClientDemoSecurityConfiguration.getAuthnAndTrustProperties().getCredential());
			ssl = new SecuredServerConnector(jetty, secureContextFactory, getHttpConnectionFactory());
			ssl.setHost(url.getHost());
			ssl.setPort(url.getPort());
		} catch (Exception e) {
			throw new RuntimeException(
					"Can not create Jetty NIO SSL connector, shouldn't happen.",
					e);
		}
		return ssl;
	}
	
	protected HttpConnectionFactory getHttpConnectionFactory()
	{
		HttpConfiguration httpConfig = new HttpConfiguration();
		httpConfig.setSendServerVersion(false);
		httpConfig.setSendXPoweredBy(false);
		return new HttpConnectionFactory(httpConfig);
	}
}	
