package org.ow2.contrail.common.oauth.authzserver.adminapi;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.contrail.common.oauth.authzserver.Utils;
import org.ow2.contrail.common.oauth.authzserver.jpa.enums.AuthorizedGrantType;
import org.ow2.contrail.common.oauth.authzserver.utils.PersistenceUtils;

import static org.junit.Assert.*;

//@Ignore
public class ClientResourceTest extends JerseyTest {

    public ClientResourceTest() throws Exception {
        super(new WebAppDescriptor.Builder("org.ow2.contrail.common.oauth.authzserver.adminapi")
                .contextPath("oauth-as").build());
    }

    @Before
    public void setUp() throws Exception {
        PersistenceUtils.createInstance("testPersistenceUnit");
    }

    @After
    public void tearDown() throws Exception {
       PersistenceUtils.getInstance().close();
       Utils.dropTestDatabase();
    }

    @Test
    public void testRegisterClient() throws JSONException {
        WebResource webResource = resource();
        String baseUri = webResource.getURI().toString();

        // register organization
        JSONObject orgData = new JSONObject();
        orgData.put("name", "My Organization");
        ClientResponse addOrgResponse = webResource.path("/organizations").post(ClientResponse.class, orgData);
        assertEquals(addOrgResponse.getStatus(), 201);
        String organizationUri = addOrgResponse.getHeaders().getFirst("Location");
        assertEquals(organizationUri, baseUri + "/organizations/1");

        // register client
        JSONObject clientData = new JSONObject();
        clientData.put("name", "My Client");
        clientData.put("callback_uri", "http://localhost:8080/myclient");
        JSONArray agtArr = new JSONArray();
        agtArr.put(AuthorizedGrantType.AUTHORIZATION_CODE);
        agtArr.put(AuthorizedGrantType.CLIENT_CREDENTIALS);
        clientData.put("authorized_grant_types", agtArr);
        clientData.put("organization_id", 1);
        JSONArray countriesArr = new JSONArray();
        clientData.put("countries", countriesArr);
        ClientResponse addClientResponse = webResource.path("/organizations/1/clients").post(ClientResponse.class, clientData);
        assertEquals(addClientResponse.getStatus(), 200);
        
//        String clientUri = addClientResponse.getHeaders().getFirst("Location");
//        System.out.println(clientUri);
//      assertEquals(clientUri, baseUri + "/organizations/1/clients/"+clientId);        
        
        JSONObject responseObj = addClientResponse.getEntity(JSONObject.class);
        String clientId = responseObj.getString("client_id");
               
        


        // get client
        JSONObject clientInfo = webResource.path("/organizations/1/clients/"+clientId).get(JSONObject.class);
        assertEquals(clientId, clientInfo.getString("client_id"));
        JSONArray agtArr1 = clientInfo.getJSONArray("authorized_grant_types");
        assertEquals(AuthorizedGrantType.AUTHORIZATION_CODE.name(), agtArr1.getString(0));
        assertEquals(AuthorizedGrantType.CLIENT_CREDENTIALS.name(), agtArr1.getString(1));

        // get all clients
        JSONArray clientsArray = webResource.path("/organizations/1/clients").get(JSONArray.class);
        System.out.println(clientsArray.toString(2));
        assertEquals(1, clientsArray.length());

       // delete client
        webResource.path("/organizations/1/clients/"+clientId).delete();

        // get all clients
        clientsArray = webResource.path("/organizations/1/clients").get(JSONArray.class);
        assertEquals(0, clientsArray.length());
    }

}
