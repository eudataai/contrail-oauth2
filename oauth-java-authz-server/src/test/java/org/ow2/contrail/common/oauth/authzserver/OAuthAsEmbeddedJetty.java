package org.ow2.contrail.common.oauth.authzserver;

import java.net.URL;

import org.eclipse.jetty.server.AbstractConnector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ssl.SslConnector;
import org.eclipse.jetty.server.ssl.SslSelectChannelConnector;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.WebAppContext;

import eu.unicore.util.configuration.ConfigurationException;
import eu.unicore.util.jetty.HttpServerProperties;
import eu.unicore.util.jetty.NIOSSLSocketConnector;

public class OAuthAsEmbeddedJetty {
	private Server jetty;
	public final static String baseUrl = "https://localhost:8443";
	public void start(String appName) {
		try {
			jetty = new Server();
			WebAppContext context = new WebAppContext();
			context.setContextPath("/" + appName);
			context.setWar("src/main/webapp");
			context.setServer(jetty);
			
			jetty.setHandler(context);
			jetty.addConnector(createSecureConnector(new URL(baseUrl)));
			jetty.start();
			jetty.join();
			jetty.setStopAtShutdown(true);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void stop() {
		try {
			jetty.stop();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) {
		OAuthAsEmbeddedJetty j = new OAuthAsEmbeddedJetty();
		j.start("oauth-as");
	}

	protected AbstractConnector createSecureConnector(URL url)
			throws ConfigurationException {
		boolean useNio = true;
		SslConnector ssl;

		System.out.println("Creating SSL NIO connector on: " + url);
		ssl = getNioSecuredConnectorInstance();

		SslContextFactory factory = ssl.getSslContextFactory();
		factory.setNeedClientAuth(false);
		factory.setWantClientAuth(false);
//		String disabledCiphers = extraSettings
//				.getValue(HttpServerProperties.DISABLED_CIPHER_SUITES);
//		if (disabledCiphers != null) {
//			disabledCiphers = disabledCiphers.trim();
//			if (disabledCiphers.length() > 1)
//				factory.setExcludeCipherSuites(disabledCiphers.split("[ ]+"));
//		}
		System.out.println("SSL protocol was set to: '" + factory.getProtocol() + "'");
		return (AbstractConnector) ssl;
	}

	/**
	 * @return an instance of NIO secure connector. It uses proper validators
	 *         and credentials and lowResourcesConnections are set to the
	 *         difference between MAX and LOW THREADS.
	 */
	protected SslSelectChannelConnector getNioSecuredConnectorInstance() {
		NIOSSLSocketConnector ssl;
		try {
			ssl = new NIOSSLSocketConnector(
					OAuthAsSecurityConfiguration.getAuthnAndTrustProperties().getValidator(),
					OAuthAsSecurityConfiguration.getAuthnAndTrustProperties().getCredential());
			ssl.setHost("localhost");
			ssl.setPort(8443);
		} catch (Exception e) {
			throw new RuntimeException(
					"Can not create Jetty NIO SSL connector, shouldn't happen.",
					e);
		}
		ssl.setLowResourcesConnections(350);
		return ssl;
	}
}
