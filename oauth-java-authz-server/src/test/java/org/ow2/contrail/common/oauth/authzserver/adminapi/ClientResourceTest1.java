package org.ow2.contrail.common.oauth.authzserver.adminapi;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;
import org.ow2.contrail.common.oauth.authzserver.jpa.enums.AuthorizedGrantType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;

/**
 * It is an integration test
 * */
public class ClientResourceTest1 {

    @Test
    public void testRegisterClient() throws JSONException {
    	Client c = new Client();
    	WebResource webResource = c.resource("http://localhost:8443/oauth-as/admin");
//    	WebResource webResource = c.resource("https://eudat-aai.fz-juelich.de:8445/oauth-as/admin");
        
        String baseUri = webResource.getURI().toString();

        // register organization
        JSONObject orgData = new JSONObject();
        orgData.put("name", "My Organization");
        System.out.println(orgData.toString(2));
        ClientResponse addOrgResponse = webResource.path("/organizations").post(ClientResponse.class, orgData);
        assertEquals(201, addOrgResponse.getStatus());
        String organizationUri = addOrgResponse.getHeaders().getFirst("Location");
        assertTrue(organizationUri.startsWith(baseUri));

        // register client
        JSONObject clientData = new JSONObject();
        clientData.put("name", "oauth-java-client-demo");
        clientData.put("callback_uri", "https://localhost:8082/oauth-authorization-flow-demo/oauth2callback");
        JSONArray agtArr = new JSONArray();
        agtArr.put(AuthorizedGrantType.AUTHORIZATION_CODE);
        agtArr.put(AuthorizedGrantType.CLIENT_CREDENTIALS);
        clientData.put("authorized_grant_types", agtArr);
        clientData.put("organization_id", 1);
        JSONArray countriesArr = new JSONArray();
        clientData.put("countries", countriesArr);
        System.out.println(clientData.toString(2));
        ClientResponse addClientResponse = webResource.path("/organizations/1/clients").post(ClientResponse.class, clientData);
        
        JSONObject response = addClientResponse.getEntity(JSONObject.class);
        System.out.println("Client ID: "+response.getString("client_id"));
        System.out.println("Client Secret: "+response.getString("client_secret"));
        
        // get client
        JSONObject clientInfo = webResource.path("/organizations/1/clients/"+response.getString("client_id")).get(JSONObject.class);
        assertNotNull(clientInfo.getString("client_id"));
        JSONArray agtArr1 = clientInfo.getJSONArray("authorized_grant_types");
        assertEquals(agtArr1.getString(0), AuthorizedGrantType.AUTHORIZATION_CODE.name());
        assertEquals(agtArr1.getString(1), AuthorizedGrantType.CLIENT_CREDENTIALS.name());

        // get all clients
        JSONArray clientsArray = webResource.path("/organizations/1/clients").get(JSONArray.class);
        System.out.println("Number of clients: "+clientsArray.length());
    }
    
    @Test
    public void testRegisterSSLClient() throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, CertificateException, FileNotFoundException, IOException, UnrecoverableKeyException, JSONException{
    	//initializing ssl client
    	ClientConfig config = new DefaultClientConfig();
		
		SSLContext ctx = SSLContext.getInstance("SSL");
		KeyStore ks = KeyStore.getInstance("PKCS12");
		FileInputStream fis = new FileInputStream(new File("src/test/resources/contrail.federation.ca.p12")); 
		ks.load(fis, "contrail".toCharArray());
		fis.close();
		
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(ks,"contrail".toCharArray());
		
		
		KeyStore tks = KeyStore.getInstance("JKS");
		FileInputStream tfis = new FileInputStream(new File("src/test/resources/truststore.jks")); 
		tks.load(tfis, "truststore".toCharArray());
		tfis.close();
		
		KeyManagerFactory tkmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		tkmf.init(ks,"contrail".toCharArray());
		
		
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(tks);
		SecureRandom se = new SecureRandom();
		ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), se);
		
		HostnameVerifier hv = new HostnameVerifier() {
			
			public boolean verify(String arg0, SSLSession arg1) {
				// TODO Auto-generated method stub
				return true;
			}
		}; 
		
		config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hv, ctx));
		Client c = Client.create(config);
//		WebResource webResource = c.resource("https://eudat-aai.fz-juelich.de:8445/oauth-as/admin");
		WebResource webResource = c.resource("https://localhost:8443/oauth-as/admin");
		
		String baseUri = webResource.getURI().toString();
		
		
		
		
		// register organization
        JSONObject orgData = new JSONObject();
        orgData.put("name", "My Organization");
        System.out.println(orgData.toString(2));
        ClientResponse addOrgResponse = webResource.path("/organizations").post(ClientResponse.class, orgData);
        System.out.println(addOrgResponse.getStatus());
//        assertEquals(201, addOrgResponse.getStatus());
//        String organizationUri = addOrgResponse.getHeaders().getFirst("Location");
//        assertEquals(organizationUri, baseUri + "/organizations/1");

        // register client
        JSONObject clientData = new JSONObject();
//        clientData.put("client_id", "oauth-java-client-demo");
        clientData.put("name", "oauth-java-client-demo");
        clientData.put("callback_uri", "https://localhost:8082/oauth-authorization-flow-demo/oauth2callback");
//        clientData.put("callback_uri", "https://eudat-aai.fz-juelich.de:8445/oauth-demo/oauth2callback");
//        clientData.put("callback_uri", "https://localhost:8445/oauth-demo/oauth2callback");
        JSONArray agtArr = new JSONArray();
        agtArr.put(AuthorizedGrantType.AUTHORIZATION_CODE);
        agtArr.put(AuthorizedGrantType.CLIENT_CREDENTIALS);
        clientData.put("authorized_grant_types", agtArr);
//        clientData.put("client_secret", "somesecret");
        clientData.put("organization_id", 1);
        JSONArray countriesArr = new JSONArray();
        clientData.put("countries", countriesArr);
        ClientResponse addClientResponse = webResource.path("/organizations/1/clients").post(ClientResponse.class, clientData);
        
        JSONObject response = addClientResponse.getEntity(JSONObject.class);
        System.out.println("Client ID: "+response.getString("client_id"));
        System.out.println("Client Secret: "+response.getString("client_secret"));
        
        
//        assertEquals(201, addClientResponse.getStatus());
//        String clientUri = addClientResponse.getHeaders().getFirst("Location");
//        assertEquals(clientUri, baseUri + "/organizations/1/clients/1");

        // get client
//        JSONObject clientInfo = webResource.path("/organizations/1/clients/1").get(JSONObject.class);
//        assertEquals(clientInfo.getString("client_id"), "oauth-java-client-demo");
//        JSONArray agtArr1 = clientInfo.getJSONArray("authorized_grant_types");
//        assertEquals(agtArr1.getString(0), AuthorizedGrantType.AUTHORIZATION_CODE.name());
//        assertEquals(agtArr1.getString(1), AuthorizedGrantType.CLIENT_CREDENTIALS.name());
		
		
	}
    
    
    @Test
    public void testRegisterSSLClientLocalhost() throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, CertificateException, FileNotFoundException, IOException, UnrecoverableKeyException, JSONException{
    	//initializing ssl client
    	ClientConfig config = new DefaultClientConfig();
		
		SSLContext ctx = SSLContext.getInstance("SSL");
		KeyStore ks = KeyStore.getInstance("PKCS12");
		FileInputStream fis = new FileInputStream(new File("src/test/resources/contrail.federation.ca.p12")); 
		ks.load(fis, "contrail".toCharArray());
		fis.close();
		
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(ks,"contrail".toCharArray());
		
		
		KeyStore tks = KeyStore.getInstance("JKS");
		FileInputStream tfis = new FileInputStream(new File("src/test/resources/truststore.jks")); 
		tks.load(tfis, "truststore".toCharArray());
		tfis.close();
		
		KeyManagerFactory tkmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		tkmf.init(ks,"contrail".toCharArray());
		
		
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(tks);
		SecureRandom se = new SecureRandom();
		ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), se);
		
		HostnameVerifier hv = new HostnameVerifier() {
			
			public boolean verify(String arg0, SSLSession arg1) {
				// TODO Auto-generated method stub
				return true;
			}
		}; 
		
		config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hv, ctx));
		Client c = Client.create(config);
		WebResource webResource = c.resource("https://localhost:8445/oauth-as/admin");
		
		String baseUri = webResource.getURI().toString();
		
		
		
		
		// register organization
        JSONObject orgData = new JSONObject();
        orgData.put("name", "My Organization");
        System.out.println(orgData.toString(2));
        ClientResponse addOrgResponse = webResource.path("/organizations").post(ClientResponse.class, orgData);
        assertEquals(201, addOrgResponse.getStatus());
        String organizationUri = addOrgResponse.getHeaders().getFirst("Location");
        assertEquals(organizationUri, baseUri + "/organizations/1");

        // register client
        JSONObject clientData = new JSONObject();
        clientData.put("client_id", "oauth-java-client-demo");
        clientData.put("name", "oauth-java-client-demo");
        clientData.put("callback_uri", "https://localhost:8445/oauth-demo/oauth2callback");
        JSONArray agtArr = new JSONArray();
        agtArr.put(AuthorizedGrantType.AUTHORIZATION_CODE);
        agtArr.put(AuthorizedGrantType.CLIENT_CREDENTIALS);
        clientData.put("authorized_grant_types", agtArr);
        clientData.put("client_secret", "somesecret");
        clientData.put("organization_id", 1);
        JSONArray countriesArr = new JSONArray();
        clientData.put("countries", countriesArr);
        System.out.println(clientData.toString(2));
        ClientResponse addClientResponse = webResource.path("/organizations/1/clients").post(ClientResponse.class, clientData);
        assertEquals(201, addClientResponse.getStatus());
        String clientUri = addClientResponse.getHeaders().getFirst("Location");
        assertEquals(clientUri, baseUri + "/organizations/1/clients/1");

        // get client
        JSONObject clientInfo = webResource.path("/organizations/1/clients/1").get(JSONObject.class);
        assertEquals(clientInfo.getString("client_id"), "oauth-java-client-demo");
        JSONArray agtArr1 = clientInfo.getJSONArray("authorized_grant_types");
        assertEquals(agtArr1.getString(0), AuthorizedGrantType.AUTHORIZATION_CODE.name());
        assertEquals(agtArr1.getString(1), AuthorizedGrantType.CLIENT_CREDENTIALS.name());
		
		
	}
    
    @Test
    public void test() throws JSONException{
    	
    	// register client
        JSONObject clientData = new JSONObject();
        clientData.put("client_id", "oauth-java-client-demo");
        clientData.put("name", "oauth-java-client-demo");
//        clientData.put("callback_uri", "https://localhost:8082/oauth-authorization-flow-demo/oauth2callback");
        clientData.put("callback_uri", "https://eudat-aai.fz-juelich.de:8445/oauth-demo/oauth2callback");
        JSONArray agtArr = new JSONArray();
        agtArr.put(AuthorizedGrantType.AUTHORIZATION_CODE);
        agtArr.put(AuthorizedGrantType.CLIENT_CREDENTIALS);
        clientData.put("authorized_grant_types", agtArr);
        clientData.put("client_secret", "somesecret");
        clientData.put("organization_id", 1);
        JSONArray countriesArr = new JSONArray();
        clientData.put("countries", countriesArr);
        System.out.println(clientData.toString(2));
    }
    
    
}
