package org.ow2.contrail.common.oauth.authzserver;

import java.util.Properties;

import eu.unicore.security.canl.AuthnAndTrustProperties;
import eu.unicore.security.canl.CredentialProperties;
import eu.unicore.security.canl.TrustedIssuersProperties.TruststoreType;
import eu.unicore.security.canl.TruststoreProperties;

public class OAuthAsSecurityConfiguration {
	public static AuthnAndTrustProperties getAuthnAndTrustProperties() {
		Properties p = new Properties();
		// Credential Properties
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_FORMAT,
				"JKS");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_KEY_LOCATION, "src/test/resources/oauth-as.jks");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_LOCATION, "src/test/resources/oauth-as.jks");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_PASSWORD, "eudat");

		// truststore properties
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_TYPE,
				TruststoreProperties.TruststoreType.keystore.toString());
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_PATH,
				"src/test/resources/oauth-as.jks");
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_TYPE,
				"jks");
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_PASSWORD,
				"eudat");
		
		AuthnAndTrustProperties auth = new AuthnAndTrustProperties(p);
		
		return auth;
	}
}
