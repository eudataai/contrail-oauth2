package org.ow2.contrail.common.oauth.authzserver.jpa.dao;

import org.ow2.contrail.common.oauth.authzserver.jpa.entities.AccessToken;
import org.ow2.contrail.common.oauth.authzserver.jpa.entities.Client;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class AccessTokenDao extends BaseDao {

    public AccessTokenDao(EntityManager em) {
        super(em);
    }
    
    /***
     * Obtain access token details based on opaque string
     * 
     * @param token an opaque string
     */
    public AccessToken findByToken(String token) {
        try {
            Query q = em.createQuery("SELECT t FROM AccessToken t WHERE t.token = :token");
            q.setParameter("token", token);
            return (AccessToken) q.getSingleResult();
        }
        catch (NoResultException e) {
            return null;
        }
    }
    
    /***
     * Retrieve corresponding OAuth2 Client based on the access token 
     * 
     * @param token an opaque string
     */
    public Client findClientByTokenId(String token){
    	try {
            Query q = em.createQuery("SELECT t FROM AccessToken t WHERE t.token = :token");
            q.setParameter("token", token);
            AccessToken tok = (AccessToken) q.getSingleResult();
            return tok.getClient();
        }
        catch (NoResultException e) {
            return null;
        }
    }
}
