package org.ow2.contrail.common.oauth.authzserver.jpa.dao;

import java.util.List;

import org.ow2.contrail.common.oauth.authzserver.jpa.entities.Client;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class ClientDao extends BaseDao {

	public ClientDao(EntityManager em) {
		super(em);
	}
	
	public List<Client> findClientsByOrgId(int orgId) {
		try {
			Query q = em
					.createQuery("SELECT c FROM Client c WHERE c.organization.id = :orgId");
			q.setParameter("orgId", orgId);
			@SuppressWarnings("unchecked")
			List<Client> lstClient = q.getResultList();
			return lstClient;
		} catch (NoResultException e) {
			return null;
		}
	}

	public Client findByPK(int clientId, int orgId) {
		Client client = em.find(Client.class, clientId);
		if (client == null || !client.getOrganization().getId().equals(orgId)) {
			return null;
		} else {
			return client;
		}
	}

	public Client find(String clientId, int orgId) {
		try {
			Query q = em
					.createQuery("SELECT c FROM Client c WHERE c.clientId = :clientId AND c.organization.id = :orgId");
			q.setParameter("clientId", clientId);
			q.setParameter("orgId", orgId);
			return (Client) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Client findByClientId(String clientId) {
		try {
			Query q = em
					.createQuery("SELECT c FROM Client c WHERE c.clientId = :clientId");
			q.setParameter("clientId", clientId);
			return (Client) q.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public int findPkById(String clientId) {
		try {
			Query q = em
					.createQuery("SELECT c FROM Client c WHERE c.clientId = :clientId");
			q.setParameter("clientId", clientId);
			Client client = (Client) q.getSingleResult();
			return client.getId();
		} catch (NoResultException e) {
			return 0;
		}
	}
}
