package org.ow2.contrail.common.oauth.client;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UnityTokenInfo implements ITokenInfo{
	private Date expireTime;
	private JSONObject jsonObj;
	private String accessToken;
	private List<String> lstScope;
	private String clientId;
	private String resOwnerId;
	
	public UnityTokenInfo(String json, String accessToken) throws JSONException {
		jsonObj = new JSONObject(json);
		this.accessToken = accessToken;
		
		Calendar c = Calendar.getInstance();
		Long time = c.getTimeInMillis()+jsonObj.getInt("exp");
		c.setTimeInMillis(time);
		
		expireTime = c.getTime();
		clientId = jsonObj.isNull("client_id") ? "Not Specified" : jsonObj.getString("client_id");
		resOwnerId = jsonObj.getString("sub");
		JSONArray jArr = jsonObj.getJSONArray("scope");
		lstScope = new ArrayList<String>();
		for (int i = 0; i<jArr.length(); i++) {
			lstScope.add(jArr.getString(i));
		}
	}
	
	public Date getExpireTime(){
		return expireTime;
	}

	@Override
	public String getClientId() {
		return clientId;
	}

	@Override
	public List<String> getScopes() {
		return lstScope;
	}

	@Override
	public String getAccessToken() {
		return accessToken;
	}

	@Override
	public String getOwnerUuid() {
		return resOwnerId;
	}
	
	@Override
	public String toString() {
		String json;
		try {
			json = jsonObj.toString(2);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		return json;
	}
	
}
