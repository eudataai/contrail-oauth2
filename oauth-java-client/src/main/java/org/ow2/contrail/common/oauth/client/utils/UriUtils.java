package org.ow2.contrail.common.oauth.client.utils;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

public class UriUtils {
	
	/**
	 * The default character set for the client ID and secret encoding.
	 */
	private static final Charset UTF8_CHARSET = Charset.forName("UTF-8");

    public static URI append (URI baseUri, String path) throws URISyntaxException {
        String url = baseUri.toString();
        if (!url.endsWith("/")) {
            url += "/";
        }
        url += (path.startsWith("/")) ? path.substring(1) : path;
        return new URI(url);
    }
    
    public static String toHTTPAuthorizationHeader(String id, String secret) {

		StringBuilder sb = new StringBuilder();

		try {
			sb.append(URLEncoder.encode(id, UTF8_CHARSET.name()));
			sb.append(':');
			sb.append(URLEncoder.encode(secret, UTF8_CHARSET.name()));

		} catch (UnsupportedEncodingException e) {

			// UTF-8 should always be supported
		}

		return "Basic " + Base64.encodeBase64String(sb.toString().getBytes(UTF8_CHARSET));
	}
    
    public static Header getHttpAuthorizationHeader(String id, String secret){
    	String val = toHTTPAuthorizationHeader(id, secret);
    	return new BasicHeader("Authorization", val);    	
    }
}
