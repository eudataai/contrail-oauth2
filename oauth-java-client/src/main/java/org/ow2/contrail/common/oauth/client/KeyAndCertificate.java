package org.ow2.contrail.common.oauth.client;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import eu.emi.security.authn.x509.impl.KeyAndCertCredential;

public class KeyAndCertificate {
	private PrivateKey privateKey;
	private X509Certificate certificate;

	public KeyAndCertificate(PrivateKey privateKey, X509Certificate certificate) {
		this.privateKey = privateKey;
		this.certificate = certificate;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public X509Certificate getCertificate() {
		return certificate;
	}

	public KeyAndCertCredential getCredential() throws KeyStoreException {
		PrivateKey pk = privateKey;

		X509Certificate caCert = certificate;

		KeyAndCertCredential cred = new KeyAndCertCredential(pk,
				new X509Certificate[] { caCert });

		return cred;
	}

	public KeyStore getPKCS12KeyStore(String alias, String keyPass) {
		KeyStore store = null;
		try {
			store = KeyStore.getInstance("PKCS12");
		store.load(null, null);
		store.setKeyEntry(alias, getCredential().getKey(),
				keyPass.toCharArray(), getCredential().getCertificateChain());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return store;
	}
}
