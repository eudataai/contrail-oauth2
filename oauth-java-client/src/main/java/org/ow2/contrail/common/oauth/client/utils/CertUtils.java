package org.ow2.contrail.common.oauth.client.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Random;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;

import eu.emi.security.authn.x509.helpers.CertificateHelpers;
import eu.emi.security.authn.x509.helpers.proxy.ProxySAMLExtension;
import eu.emi.security.authn.x509.helpers.proxy.X509v3CertificateBuilder;
import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;

public class CertUtils {

	public static KeyPair generateKeyPair(final String algorithm,
			final int keylen) throws NoSuchAlgorithmException {

		KeyPairGenerator kpGen = KeyPairGenerator.getInstance(algorithm);
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
		kpGen.initialize(keylen, random);
		return kpGen.generateKeyPair();
	}

	public static PKCS10CertificationRequest _createCSR(final KeyPair keyPair,
			final String subject, final String signatureAlgorithm)
			throws Exception {

		PKCS10CertificationRequestBuilder p10Builder = new JcaPKCS10CertificationRequestBuilder(
				new X500Principal(subject), keyPair.getPublic());
		JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder(
				signatureAlgorithm);
		ContentSigner signer = csBuilder.build(keyPair.getPrivate());
		PKCS10CertificationRequest csr = p10Builder.build(signer);
		
		return csr;
	}
	
	public static PKCS10CertificationRequest createCSR(final KeyPair keyPair,
			final String subject, final String signatureAlgorithm)
			throws Exception {

		PKCS10CertificationRequestBuilder p10Builder = new JcaPKCS10CertificationRequestBuilder(
				new X500Principal(subject), keyPair.getPublic());
		JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder(
				signatureAlgorithm);
		ContentSigner signer = csBuilder.build(keyPair.getPrivate());
		PKCS10CertificationRequest csr = p10Builder.build(signer);
		
		return csr;
	}

	public static X509Certificate readCertificate(InputStreamReader isr)
			throws IOException, CertificateException {
		PEMParser pemReader = new PEMParser(isr);
		Object o = pemReader.readObject();
		pemReader.close();
		if (o == null) {
			throw new IOException(
					"Failed to read PEM object from input stream.");

		}
		X509CertificateHolder certHolder = (X509CertificateHolder) o;
		X509Certificate cert = new JcaX509CertificateConverter().setProvider(
				"BC").getCertificate(certHolder);
		return cert;
	}
	
	
	

	
}
