package org.ow2.contrail.common.oauth.rp;

import java.io.ByteArrayOutputStream;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicHeader;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.ow2.contrail.common.oauth.client.ITokenInfo;
import org.ow2.contrail.common.oauth.client.IUserInfo;
import org.ow2.contrail.common.oauth.client.TokenInfo;
import org.ow2.contrail.common.oauth.client.TokenValidationProtocol;
import org.ow2.contrail.common.oauth.client.UnityTokenInfo;
import org.ow2.contrail.common.oauth.client.UnityUserInfo;
import org.ow2.contrail.common.oauth.client.utils.UriUtils;

import eu.unicore.security.canl.AuthnAndTrustProperties;
import eu.unicore.util.Log;
import eu.unicore.util.httpclient.HttpUtils;

public class UnityTokenValidator extends AbstractTokenValidator {
	static Logger log = Log.getLogger("OAuth-Client", UnityTokenValidator.class);
	public UnityTokenValidator(String address, AuthnAndTrustProperties props) {
		super(address, props);
	}

	@Override
	public ITokenInfo verifyToken(String accessToken, String bearerId)
			throws Exception {
		HttpGet httpGet = new HttpGet(getTokenValAddress());
		BasicHeader bh = new BasicHeader("Authorization", "Bearer "+accessToken);
		
		httpGet.setHeader(bh);
		
		log.info("Sending request to Unity server at: "+endpointUri);
		
		HttpResponse response = httpClient.execute(httpGet);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			
			response.getEntity().writeTo(baos);
			String json = baos.toString();
			log.info("Response from Unity: "+json);
			try {
				return new UnityTokenInfo(json, accessToken);
			} catch (Exception e) {
				throw new Exception(
						String.format(
								"Invalid response received from the OAuth authorization server '%s': %s",
								endpointUri, e.getMessage()));
			} 
			
		} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
			throw new InvalidOAuthTokenException(
					"You are not authorized to access the requested resource.");
		} else {
			throw new Exception(
					String.format(
							"Unexpected response received from the OAuth authorization server '%s': %s",
							endpointUri, response.getStatusLine().toString()));
		}
		
	}

	@Override
	public String getTokenValAddress() {
		return endpointUri;
	}

	@Override
	public String getAccessTokenValidationProtocol() {
		return TokenValidationProtocol.unity.toString();
	}

	@Override
	public IUserInfo getUserInfo(String accessToken) throws Exception {
		String userInfoURI = endpointUri.replace("/tokeninfo", "/userinfo");
		HttpGet httpGet = new HttpGet(userInfoURI);
		BasicHeader bh = new BasicHeader("Authorization", "Bearer "+accessToken);
		
		httpGet.setHeader(bh);
		
		log.info("Sending request to Unity server at: "+userInfoURI);
		
		HttpResponse response = httpClient.execute(httpGet);
		
		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			response.getEntity().writeTo(baos);
			String json = baos.toString();
			log.info("Response from Unity: "+new JSONObject(json).toString(2));
			try {
				return new UnityUserInfo(json);
			} catch (Exception e) {
				throw new Exception(
						String.format(
								"Invalid response received from the OAuth authorization server '%s': %s",
								userInfoURI, e.getMessage()));
			}
		} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
			throw new InvalidOAuthTokenException(
					"You are not authorized to access the requested resource.");
		} else {
			throw new Exception(
					String.format(
							"Unexpected response received from the OAuth authorization server '%s': %s",
							userInfoURI, response.getStatusLine().toString()));
		}
	}

}
