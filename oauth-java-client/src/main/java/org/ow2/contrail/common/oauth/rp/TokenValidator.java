package org.ow2.contrail.common.oauth.rp;

import javax.servlet.http.HttpServletRequest;

import org.ow2.contrail.common.oauth.client.ITokenInfo;
import org.ow2.contrail.common.oauth.client.IUserInfo;
import org.ow2.contrail.common.oauth.client.TokenInfo;
import org.ow2.contrail.common.oauth.client.TokenValidationProtocol;

public interface TokenValidator {
	public ITokenInfo verifyToken(String accessToken) throws Exception;
	public ITokenInfo verifyToken(String accessToken, String bearerId) throws Exception;
	public ITokenInfo verifyToken(HttpServletRequest httpReq) throws Exception;
	public IUserInfo getUserInfo(String accessToken) throws Exception;
	public String getTokenValAddress();
	public String getAccessTokenValidationProtocol();
	public static class InvalidOAuthTokenException extends Exception {
		public InvalidOAuthTokenException(String message) {
			super(message);
		}
	}
	
	public static class InvalidCertificateException extends Exception {
		public InvalidCertificateException(String message) {
			super(message);
		}
	}
}
