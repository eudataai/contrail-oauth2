package org.ow2.contrail.common.oauth.client;

public enum TokenValidationProtocol {
	unity, contrail, mitre, rcauth;
	public static TokenValidationProtocol fromString(String protocol) {
		if (protocol != null) {
			protocol = protocol.trim().toLowerCase();
			for (TokenValidationProtocol b : TokenValidationProtocol.values()) {
				if (protocol.equalsIgnoreCase(b.name())) {
					return b;
				}
			}
		}
		return null;
	}
}
