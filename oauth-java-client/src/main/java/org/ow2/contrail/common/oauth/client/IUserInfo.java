package org.ow2.contrail.common.oauth.client;

import org.json.JSONObject;

public interface IUserInfo {
	public String getUserName();
	public String getUserId();
	public String getEmail();
	public String getName();
	public String getDistinguishedName();
}
