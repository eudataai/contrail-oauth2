package org.ow2.contrail.common.oauth.client;

import java.util.Date;
import java.util.List;

/**
 * An interface to support multiple type of Access tokens
 * */
public interface ITokenInfo {
	public String getClientId();
	public String getAccessToken();
	public List<String> getScopes();
	public Date getExpireTime();
	public String getOwnerUuid();
}
