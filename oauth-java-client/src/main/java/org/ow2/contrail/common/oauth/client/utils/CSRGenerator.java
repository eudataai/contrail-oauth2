package org.ow2.contrail.common.oauth.client.utils;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.bc.BcRSAContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;

import eu.emi.security.authn.x509.helpers.CertificateHelpers;
import eu.emi.security.authn.x509.impl.CertificateUtils;

public class CSRGenerator {
	private KeyPair kp;
	static {
		CertificateUtils.configureSecProvider();
	}

	public PKCS10CertificationRequest generate(String userDN) throws NoSuchAlgorithmException, InvalidKeyException, IOException, OperatorCreationException {
		// generate key pair with the same algo. the ca is generated with
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
		// put in options
		kpg.initialize(4096);
		KeyPair pair = kpg.generateKeyPair();
		this.kp = pair;
		X500Principal subjectDN = new X500Principal(userDN);

		SubjectPublicKeyInfo publicKeyInfo;
		try {
			publicKeyInfo = SubjectPublicKeyInfo
					.getInstance(new ASN1InputStream(pair.getPublic()
							.getEncoded()).readObject());
		} catch (IOException e) {
			throw new InvalidKeyException("Can not parse the public key"
					+ "being included in the short lived certificate", e);
		}

		X500Name subjectX500Name = CertificateHelpers.toX500Name(subjectDN);

		PKCS10CertificationRequestBuilder builder = new PKCS10CertificationRequestBuilder(
				subjectX500Name, publicKeyInfo);

		// generating a csr
		AlgorithmIdentifier signatureAi = new AlgorithmIdentifier(
				OIWObjectIdentifiers.sha1WithRSA);
		AlgorithmIdentifier hashAi = new AlgorithmIdentifier(
				OIWObjectIdentifiers.idSHA1);
		BcRSAContentSignerBuilder csBuilder = new BcRSAContentSignerBuilder(
				signatureAi, hashAi);
		AsymmetricKeyParameter pkParam = PrivateKeyFactory.createKey(pair
				.getPrivate().getEncoded());
		
		ContentSigner signer = csBuilder.build(pkParam);
		
		PKCS10CertificationRequest csr = builder.build(signer);
		return csr;
	}
	
	public KeyPair getKeyPair(){
		return kp;
	}
}
