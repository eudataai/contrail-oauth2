package org.ow2.contrail.common.oauth.rp;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.ow2.contrail.common.oauth.client.IUserInfo;
import org.ow2.contrail.common.oauth.client.TokenInfo;
import org.ow2.contrail.common.oauth.client.TokenValidationProtocol;

import eu.emi.security.authn.x509.X509CertChainValidatorExt;
import eu.emi.security.authn.x509.helpers.BinaryCertChainValidator;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
import eu.unicore.security.canl.AuthnAndTrustProperties;
import eu.unicore.util.Log;
import eu.unicore.util.httpclient.DefaultClientConfiguration;
import eu.unicore.util.httpclient.HttpUtils;
import eu.unicore.util.httpclient.ServerHostnameCheckingMode;

public class ContrailTokenValidator extends AbstractTokenValidator {
	static Logger log = Log.getLogger("OAuth-Client", ContrailTokenValidator.class);

	public ContrailTokenValidator(String address, AuthnAndTrustProperties authProps) {
		super(address, authProps);
	}

	public TokenInfo verifyToken(String accessToken) throws Exception {
		return verifyToken(accessToken, null);
	}

	@Override
	public TokenInfo verifyToken(String accessToken, String bearerId)
			throws Exception {
		log.debug("Creating secure http client with hostname: "+endpointUri);
		
		log.debug("creating http post: "+endpointUri);
		HttpPost postRequest = new HttpPost(endpointUri);
		log.debug("Setting-up post http request");
		log.debug("Access token: "+accessToken);
		List<NameValuePair> formParams = new ArrayList<NameValuePair>();
		formParams.add(new BasicNameValuePair("access_token", accessToken));
		if (bearerId != null) {
			formParams.add(new BasicNameValuePair("bearer_id", bearerId));
		}

		UrlEncodedFormEntity entity = null;
		try {
			entity = new UrlEncodedFormEntity(formParams, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new Exception("Wrongly defined HTTP parameters", e);
		}
		postRequest.setEntity(entity);
		
		HttpResponse response = httpClient.execute(postRequest);
		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			response.getEntity().writeTo(baos);
			String json = baos.toString();

			try {
				return new TokenInfo(json);
			} catch (Exception e) {
				throw new Exception(
						String.format(
								"Invalid response received from the OAuth authorization server '%s': %s",
								endpointUri, e.getMessage()));
			}
		} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
			throw new InvalidOAuthTokenException(
					"You are not authorized to access the requested resource.");
		} else {
			throw new Exception(
					String.format(
							"Unexpected response received from the OAuth authorization server '%s': %s",
							endpointUri, response.getStatusLine().toString()));
		}

	}

	@Override
	public String getTokenValAddress() {
		return endpointUri;
	}
	
	private HttpClient getClient(){
		HttpUtils.getSSLConnectionManager(clientCfg);
		return null;
	}
	
	
	@Override
	public TokenInfo verifyToken(HttpServletRequest httpRequest)
			throws Exception {
		log.debug("Verifying access token");
		System.out.println("Verifying access token");
		// extract access token from the Authorization header
		String authHeader = httpRequest.getHeader("Authorization");
		if (authHeader == null) {
			throw new ContrailTokenValidator.InvalidOAuthTokenException(
					"The Authorization header is missing.");
		}

		Pattern authHeaderPattern = Pattern.compile("^Bearer ([\\w-]+)$");
		Matcher m = authHeaderPattern.matcher(authHeader);
		if (!m.find()) {
			throw new ContrailTokenValidator.InvalidOAuthTokenException(
					"Invalid Authorization header.");
		}
		String accessToken = m.group(1);

		return verifyToken(accessToken);

	}

	public static class InvalidOAuthTokenException extends Exception {
		public InvalidOAuthTokenException(String message) {
			super(message);
		}
	}

	public static class InvalidCertificateException extends Exception {
		public InvalidCertificateException(String message) {
			super(message);
		}
	}

	@Override
	public String getAccessTokenValidationProtocol() {
		return TokenValidationProtocol.contrail.toString();
	}

	@Override
	public IUserInfo getUserInfo(String accessToken) throws Exception {
		return null;
	}

}
