package org.ow2.contrail.common.oauth.client;

import org.json.JSONException;
import org.json.JSONObject;

public class UnityUserInfo implements IUserInfo {
	private String userName = null;
	private String id = null;
	private JSONObject jsonObj = null;
	private String email = null;
	private String name = null;
	private String distinguishedName = null;
	public UnityUserInfo(String jsonStr) throws JSONException {
		try {

			jsonObj = new JSONObject(jsonStr);
			this.userName = jsonObj.has("userName") ? jsonObj
					.getString("userName") : null;
			this.id = jsonObj.has("unity:persistent") ? jsonObj
					.getString("unity:persistent") : null;
			if (jsonObj.has("email") && jsonObj.isNull("email")) {
				email = jsonObj.getString("email");
			} else {
				this.email = null;
			}
			
			if(jsonObj.has("name") && !jsonObj.isNull("name")) {
				name = jsonObj.getString("name");
			}
			
			//distinguished name
			if(jsonObj.has("urn:oid:2.5.4.49") && !jsonObj.isNull("urn:oid:2.5.4.49")) {
				distinguishedName = jsonObj.getString("urn:oid:2.5.4.49");
			}
		} catch (Exception e) {
			throw new JSONException(e);
		}

	}

	@Override
	public String getUserId() {
		return id;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public String toString() {
		String str = null;
		try {
			str = jsonObj.toString(2);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return str;
	}

	@Override
	public String getUserName() {
		return userName;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDistinguishedName() {
		return distinguishedName;
	}
}
