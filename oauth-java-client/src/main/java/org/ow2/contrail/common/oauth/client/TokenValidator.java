package org.ow2.contrail.common.oauth.client;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.SchemeSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;

import eu.emi.security.authn.x509.impl.KeyAndCertCredential;

import javax.servlet.http.HttpServletRequest;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.net.URI;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TokenValidator {
	private URI endpointUri;
	private String keystoreFile;
	private String keystorePass;
	private String truststoreFile;
	private String truststorePass;
	private String keystoreType;
	private KeyAndCertCredential cred;

	/**
	 * @param  endpointUri
	 * @param keystoreFile
	 * @param keystorePass
	 * @param keystoreType
	 * @param truststoreFile
	 * @param truststorePass
	 * */
	public TokenValidator(URI endpointUri, String keystoreFile,
			String keystorePass, String keystoreType, String truststoreFile, String truststorePass) {
		this.endpointUri = endpointUri;
		this.keystoreFile = keystoreFile;
		this.keystorePass = keystorePass;
		this.keystoreType = keystoreType;
		this.truststoreFile = truststoreFile;
		this.truststorePass = truststorePass;
		
	}
	
	
	public TokenValidator(URI endpointUri, KeyAndCertCredential credential) {
		this.endpointUri = endpointUri;
		this.cred = credential;
		
	}
	
	public TokenValidator(URI endpointUri, String keystoreFile,
			String keystorePass, String truststoreFile, String truststorePass) {
		this.endpointUri = endpointUri;
		this.keystoreFile = keystoreFile;
		this.keystorePass = keystorePass;
		this.truststoreFile = truststoreFile;
		this.truststorePass = truststorePass;
		this.keystoreType = "JKS";
		
	}

	public TokenValidator(URI endpointUri) {
		this.endpointUri = endpointUri;
	}

	public TokenInfo checkToken(HttpServletRequest httpRequest)
			throws Exception {
		// extract access token from the Authorization header
		String authHeader = httpRequest.getHeader("Authorization");
		if (authHeader == null) {
			throw new TokenValidator.InvalidOAuthTokenException(
					"The Authorization header is missing.");
		}

		Pattern authHeaderPattern = Pattern.compile("^Bearer ([\\w-]+)$");
		Matcher m = authHeaderPattern.matcher(authHeader);
		if (!m.find()) {
			throw new TokenValidator.InvalidOAuthTokenException(
					"Invalid Authorization header.");
		}
		String accessToken = m.group(1);

		return checkToken(accessToken);

	}

	public TokenInfo checkToken(String accessToken, String bearerId)
			throws Exception {

		HttpPost postRequest = new HttpPost(endpointUri);
		List<NameValuePair> formParams = new ArrayList<NameValuePair>();
		formParams.add(new BasicNameValuePair("access_token", accessToken));
		formParams.add(new BasicNameValuePair("bearer_id", bearerId));
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams,
				"UTF-8");
		postRequest.setEntity(entity);

		// read in the keystore from the filesystem, this should contain a
		// single keypair
		KeyStore clientKeyStore = KeyStore.getInstance(this.keystoreType);
		clientKeyStore.load(new FileInputStream(keystoreFile),
				keystorePass.toCharArray());

		// read in the truststore from the filesystem, this should contain a
		// single keypair
		KeyStore trustStore = KeyStore.getInstance("JKS");
		trustStore.load(new FileInputStream(truststoreFile),
				truststorePass.toCharArray());

		// set up the socketfactory, to use our keystore for client
		// authentication.
		SSLSocketFactory socketFactory = new SSLSocketFactory(
				SSLSocketFactory.TLS, clientKeyStore, keystorePass, trustStore,
				null, null, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

		// create and configure scheme registry
		SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("https", endpointUri.getPort(),
				socketFactory));

		// create a client connection manager to use in creating httpclients
		ThreadSafeClientConnManager mgr = new ThreadSafeClientConnManager(
				registry);

		// create the client based on the manager, and use it to make the call
		HttpClient httpClient = new DefaultHttpClient(mgr);
		HttpResponse response = httpClient.execute(postRequest);

		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			response.getEntity().writeTo(baos);
			String json = baos.toString();

			try {
				return new TokenInfo(json);
			} catch (Exception e) {
				throw new Exception(
						String.format(
								"Invalid response received from the OAuth authorization server '%s': %s",
								endpointUri, e.getMessage()));
			}
		} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
			throw new InvalidOAuthTokenException(
					"You are not authorized to access the requested resource.");
		} else {
			throw new Exception(
					String.format(
							"Unexpected response received from the OAuth authorization server '%s': %s",
							endpointUri, response.getStatusLine().toString()));
		}
	}
	
	/**
	 * Validate token against token endpoint with an access token
	 * 
	 * @param accessToken 
	 * */
	public TokenInfo checkToken(String accessToken)
			throws Exception {

		HttpPost postRequest = new HttpPost(endpointUri);
		List<NameValuePair> formParams = new ArrayList<NameValuePair>();
		formParams.add(new BasicNameValuePair("access_token", accessToken));
//		formParams.add(new BasicNameValuePair("bearer_id", bearerId));
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams,
				"UTF-8");
		postRequest.setEntity(entity);

		// read in the keystore from the filesystem, this should contain a
		// single keypair
		KeyStore clientKeyStore = KeyStore.getInstance(this.keystoreType);
		clientKeyStore.load(new FileInputStream(keystoreFile),
				keystorePass.toCharArray());

		// read in the truststore from the filesystem, this should contain a
		// single keypair
		KeyStore trustStore = KeyStore.getInstance("JKS");
		trustStore.load(new FileInputStream(truststoreFile),
				truststorePass.toCharArray());

		// set up the socketfactory, to use our keystore for client
		// authentication.
		SSLSocketFactory socketFactory = new SSLSocketFactory(
				SSLSocketFactory.TLS, clientKeyStore, this.keystorePass, trustStore,
				null, null, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

		// create and configure scheme registry
		SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("https", endpointUri.getPort(),
				socketFactory));

		// create a client connection manager to use in creating httpclients
		ThreadSafeClientConnManager mgr = new ThreadSafeClientConnManager(
				registry);

		// create the client based on the manager, and use it to make the call
		HttpClient httpClient = new DefaultHttpClient(mgr);
		HttpResponse response = httpClient.execute(postRequest);

		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			response.getEntity().writeTo(baos);
			String json = baos.toString();

			try {
				return new TokenInfo(json);
			} catch (Exception e) {
				throw new Exception(
						String.format(
								"Invalid response received from the OAuth authorization server '%s': %s",
								endpointUri, e.getMessage()));
			}
		} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
			throw new InvalidOAuthTokenException(
					"You are not authorized to access the requested resource.");
		} else {
			throw new Exception(
					String.format(
							"Unexpected response received from the OAuth authorization server '%s': %s",
							endpointUri, response.getStatusLine().toString()));
		}
	}

	public TokenInfo checkTokenHttp(String accessToken, String bearerId)
			throws Exception {

		HttpPost postRequest = new HttpPost(endpointUri);
		List<NameValuePair> formParams = new ArrayList<NameValuePair>();
		formParams.add(new BasicNameValuePair("access_token", accessToken));
		formParams.add(new BasicNameValuePair("bearer_id", bearerId));
		UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams,
				"UTF-8");
		postRequest.setEntity(entity);

		PlainSocketFactory socketFactory = new PlainSocketFactory();

		// create and configure scheme registry
		SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("http", endpointUri.getPort(),
				socketFactory));

		// create a client connection manager to use in creating httpclients
		ThreadSafeClientConnManager mgr = new ThreadSafeClientConnManager(
				registry);

		HttpClient c = new DefaultHttpClient(mgr);
		HttpResponse response = c.execute(postRequest);

		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			response.getEntity().writeTo(baos);
			String json = baos.toString();

			try {
				return new TokenInfo(json);
			} catch (Exception e) {
				throw new Exception(
						String.format(
								"Invalid response received from the OAuth authorization server '%s': %s",
								endpointUri, e.getMessage()));
			}
		} else if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
			throw new InvalidOAuthTokenException(
					"You are not authorized to access the requested resource.");
		} else {
			throw new Exception(
					String.format(
							"Unexpected response received from the OAuth authorization server '%s': %s",
							endpointUri, response.getStatusLine().toString()));
		}
	}

	public static class InvalidOAuthTokenException extends Exception {
		public InvalidOAuthTokenException(String message) {
			super(message);
		}
	}

	public static class InvalidCertificateException extends Exception {
		public InvalidCertificateException(String message) {
			super(message);
		}
	}
}
