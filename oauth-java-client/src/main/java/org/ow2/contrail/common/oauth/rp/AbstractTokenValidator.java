package org.ow2.contrail.common.oauth.rp;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.log4j.Logger;
import org.ow2.contrail.common.oauth.client.ITokenInfo;
import org.ow2.contrail.common.oauth.client.TokenInfo;
import org.ow2.contrail.common.oauth.client.TokenValidationProtocol;

import eu.emi.security.authn.x509.X509CertChainValidatorExt;
import eu.emi.security.authn.x509.helpers.BinaryCertChainValidator;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
import eu.unicore.security.canl.AuthnAndTrustProperties;
import eu.unicore.util.Log;
import eu.unicore.util.httpclient.DefaultClientConfiguration;
import eu.unicore.util.httpclient.HttpClientProperties;
import eu.unicore.util.httpclient.HttpUtils;
import eu.unicore.util.httpclient.ServerHostnameCheckingMode;

public abstract class AbstractTokenValidator implements TokenValidator{
	private static Logger log = Log.getLogger("OAuth2.0-Client", AbstractTokenValidator.class);
	protected String endpointUri;
	protected TokenValidationProtocol protocol;
	protected HttpClient httpClient;
	protected String accessToken;
	protected String bearerId;
	protected DefaultClientConfiguration clientCfg = null;
	
	public AbstractTokenValidator(String address, AuthnAndTrustProperties authProps) {
		clientCfg = new DefaultClientConfiguration();
		clientCfg.setCredential(authProps.getCredential());
//		clientCfg.setValidator(authProps.getValidator());
		clientCfg.setValidator(new BinaryCertChainValidator(true));
		clientCfg.setSslEnabled(true);
		clientCfg.setSslAuthn(false);
		clientCfg.setServerHostnameCheckingMode(ServerHostnameCheckingMode.NONE);
		clientCfg.setHttpClientProperties(getHttpClientProperties());
		this.endpointUri = address;
		this.httpClient = HttpUtils.createClient(endpointUri, clientCfg);
	}
	
	private HttpClientProperties getHttpClientProperties() {
		Properties prop = new Properties();
		prop.setProperty(HttpClientProperties.CONNECTION_CLOSE, "true");
		prop.setProperty(HttpClientProperties.MAX_HOST_CONNECTIONS, "60");
		prop.setProperty(HttpClientProperties.MAX_TOTAL_CONNECTIONS, "200");
		HttpClientProperties httpProps = new HttpClientProperties(prop);
		return httpProps;
	}

	@Override
	public ITokenInfo verifyToken(String accessToken) throws Exception {
		return verifyToken(accessToken, null);
	}
	
	@Override
	public ITokenInfo verifyToken(HttpServletRequest httpRequest)
			throws Exception {
		log.debug("Verifying access token");
		// extract access token from the Authorization header
		String authHeader = httpRequest.getHeader("Authorization");
		if (authHeader == null) {
			log.error("The Authorization header is missing.");
			throw new TokenValidator.InvalidOAuthTokenException(
					"The Authorization header is missing.");
		}

		Pattern authHeaderPattern = Pattern.compile("^Bearer ([\\w-]+)$");
		Matcher m = authHeaderPattern.matcher(authHeader);
		if (!m.find()) {
			throw new TokenValidator.InvalidOAuthTokenException(
					"Invalid Authorization header.");
		}
		String accessToken = m.group(1);

		return verifyToken(accessToken);

	}

}
