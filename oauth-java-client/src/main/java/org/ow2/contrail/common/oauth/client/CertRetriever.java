package org.ow2.contrail.common.oauth.client;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.ow2.contrail.common.oauth.client.utils.CSRGenerator;
import org.ow2.contrail.common.oauth.client.utils.CertUtils;

public class CertRetriever {
    private static Logger log = Logger.getLogger(CertRetriever.class);

    private URI endpointUri;
    private String keystoreFile;
    private String keystorePass;
    private String keystoreType;
    private String truststoreFile;
    private String truststorePass;
    private String clientId;
    private String clientSecret;

    public CertRetriever(URI endpointUri) throws Exception {
        if (endpointUri.getScheme().equals("https")) {
            throw new Exception("Keystore and truststore are required for https connection.");
        }
        this.endpointUri = endpointUri;
    }
    
    public CertRetriever(URI endpointUri,
                         String keystoreFile, String keystorePass, String keystoreType,
                         String truststoreFile, String truststorePass, String clientId, String clientSecret) {
        this.endpointUri = endpointUri;
        this.keystoreFile = keystoreFile;
        this.keystorePass = keystorePass;
        this.keystoreType = keystoreType;
        this.truststoreFile = truststoreFile;
        this.truststorePass = truststorePass;
        this.clientId = clientId;
        this.clientSecret = clientSecret;

        if (Security.getProvider("BC") == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }
    
    /***
     * This function prepares a request (containing the access token and CSR) 
     * to fetch short-lived x509 from the online ca server  
     *  
     * @param accessToken a transient delegation token 
     */
    public KeyAndCertificate retrieveCert(String accessToken) throws Exception {
        log.debug("Retrieving user certificate from the ca-server at " + endpointUri);
        log.debug("Generating private/public key pair and CSR.");
		
		// TODO: subject?
        String subject = String.format("CN=%s", "TestUser");
        
        CSRGenerator csrGen = new CSRGenerator();
        PKCS10CertificationRequest csr = csrGen.generate(subject);
        
        log.debug("Encoding CSR into PEM format.");
        StringWriter writer = new StringWriter();
        PEMWriter pemWriter = new PEMWriter(writer);
        pemWriter.writeObject(csr);
        pemWriter.flush();
        pemWriter.close();

        String pemEncoded = writer.toString();

        HttpPost request = new HttpPost(endpointUri);
        log.debug("Using access token: " + accessToken);
        request.addHeader("Authorization", String.format("Bearer %s", accessToken));
        request.addHeader("Accept-Encoding", "identity");

        List<NameValuePair> formParams = new ArrayList<NameValuePair>();
        formParams.add(new BasicNameValuePair("certificate_request", pemEncoded));
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, "UTF-8");
        request.setEntity(entity);
        
        HttpClient httpClient = null;
        if (endpointUri.getScheme().equals("https")) {
        	
        	httpClient = new DefaultHttpClient();
        	
            log.debug("Setting up SSL-enabled HttpClient. ");
            // read in the keystore from the filesystem, this should contain a single keypair
            KeyStore clientKeyStore = KeyStore.getInstance(this.keystoreType);
            clientKeyStore.load(new FileInputStream(keystoreFile), keystorePass.toCharArray());
         // kmf
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            //FIXME
            kmf.init(clientKeyStore, keystorePass.toCharArray());
            // read in the truststore from the filesystem, this should contain a single keypair
            KeyStore trustStore = KeyStore.getInstance("JKS");
            trustStore.load(new FileInputStream(truststoreFile), truststorePass.toCharArray());
         // tmf
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(trustStore);
            
         // SSL
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            
         // socket
            SSLSocketFactory socketFactory = new SSLSocketFactory(ctx, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            
            httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", endpointUri.getPort(), socketFactory));
            
                    
         
        }
        else {
            log.debug("Setting up non-SSL HttpClient.");
            httpClient = new DefaultHttpClient();
        }

        log.debug("Sending request to " + endpointUri);
        HttpResponse response = httpClient.execute(request);
        log.debug("Received response from the ca-server: " + response.getStatusLine());
        HttpEntity responseEntity = response.getEntity();

        if (response.getStatusLine().getStatusCode() != 200) {
            Scanner scanner = new Scanner(responseEntity.getContent()).useDelimiter("\\A");
            String content = scanner.hasNext() ? scanner.next() : "No content in response.";
            log.error(String.format("Unexpected response from the ca-server:\\n%s\\n%s",
                    response.getStatusLine(), content));
            throw new Exception("Unexpected response from the ca-server: " + response.getStatusLine());
        }

        InputStreamReader isr = null;
        X509Certificate cert;
        try {
            log.debug("Loading certificate from PEM.");
            isr = new InputStreamReader(responseEntity.getContent());
            cert = CertUtils.readCertificate(isr);
            log.debug("The certificate was obtained successfully.");

            return new KeyAndCertificate(csrGen.getKeyPair().getPrivate(), cert);
        }
        catch (Exception e) {
            log.error("Invalid response received from the ca-server - failed to load certificate from PEM.", e);
            throw new Exception("Invalid response received from the ca-server - failed to load certificate from PEM: " +
                    e.getMessage());
        }
        finally {
            if (isr != null)
                isr.close();
        }
    }
    
    private HttpClient getHttpClient() throws Exception{
        HttpClient httpClient = null;
        if (endpointUri.getScheme().equals("https")) {
        	
        	httpClient = new DefaultHttpClient();
        	
            log.debug("Setting up SSL-enabled HttpClient. ");
            // read in the keystore from the filesystem, this should contain a single keypair
            KeyStore clientKeyStore = KeyStore.getInstance(this.keystoreType);
            clientKeyStore.load(new FileInputStream(keystoreFile), keystorePass.toCharArray());
         // kmf
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            //FIXME
            kmf.init(clientKeyStore, keystorePass.toCharArray());
            // read in the truststore from the filesystem, this should contain a single keypair
            KeyStore trustStore = KeyStore.getInstance("JKS");
            trustStore.load(new FileInputStream(truststoreFile), truststorePass.toCharArray());
         // tmf
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(trustStore);
            
         // SSL
            SSLContext ctx = SSLContext.getInstance("TLS");
            ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            
         // socket
            SSLSocketFactory socketFactory = new SSLSocketFactory(ctx, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            
            Integer port = ((endpointUri.getPort()) < 0) ? 443 : endpointUri.getPort();
            
            log.debug("Port number: "+port);
            
            httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", port, socketFactory));
            
        }
        else {
            log.debug("Setting up non-SSL HttpClient.");
            httpClient = new DefaultHttpClient();
        }

        return httpClient;
    }
    
	public String retrieveProxyCert(String accessToken) throws Exception{
		log.debug("Access token: " + accessToken);
        log.debug("Client Id: " + clientId);
		HttpPost request = new HttpPost(endpointUri);
        //request.addHeader("Authorization", String.format("Bearer %s", accessToken));
        //request.addHeader("Accept-Encoding", "identity");
        List<NameValuePair> formParams = new ArrayList<NameValuePair>();
        formParams.add(new BasicNameValuePair("client_id", clientId));
        formParams.add(new BasicNameValuePair("client_secret", clientSecret));
        formParams.add(new BasicNameValuePair("access_token", accessToken));
        UrlEncodedFormEntity entity = null;
		entity = new UrlEncodedFormEntity(formParams, "UTF-8");
        request.setEntity(entity);
		
        log.debug("Sending request to " + endpointUri);
        HttpResponse response = getHttpClient().execute(request);
        log.debug("Received response from the ca-server: " + response.getStatusLine());
        HttpEntity responseEntity = response.getEntity();
       
        if (response.getStatusLine().getStatusCode() != 200) {
            Scanner scanner = new Scanner(responseEntity.getContent()).useDelimiter("\\A");
            String content = scanner.hasNext() ? scanner.next() : "No content in response.";
            log.error(String.format("Unexpected response from the ca-server:\\n%s\\n%s",
                    response.getStatusLine(), content));
            throw new Exception("Unexpected response from the ca-server: " + response.getStatusLine());
        }
       
        System.out.println(responseEntity.getContent());
        InputStreamReader isr = null;
        BufferedReader br = null;
        isr = new InputStreamReader(responseEntity.getContent());
        return IOUtils.toString(responseEntity.getContent());
	}
    
}
