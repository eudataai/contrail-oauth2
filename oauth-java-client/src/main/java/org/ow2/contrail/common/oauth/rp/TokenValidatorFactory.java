package org.ow2.contrail.common.oauth.rp;

import org.ow2.contrail.common.oauth.client.TokenValidationProtocol;

import eu.unicore.security.canl.AuthnAndTrustProperties;

public class TokenValidatorFactory {
	public static TokenValidator newInstance(TokenValidationProtocol protocol, String address, AuthnAndTrustProperties props) throws Exception{
		TokenValidator validator = null;
		switch (protocol) {
		case contrail:
			validator = new ContrailTokenValidator(address, props);
			break;
		case unity:
			validator = new UnityTokenValidator(address, props);
			break;
		default:
			break;
		}
		return validator;
	}
}
