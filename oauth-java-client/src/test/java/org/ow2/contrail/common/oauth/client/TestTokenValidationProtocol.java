package org.ow2.contrail.common.oauth.client;

import static  org.junit.Assert.*;
import org.junit.Test;
import org.ow2.contrail.common.oauth.client.TokenValidationProtocol;

public class TestTokenValidationProtocol {
	@Test
	public void testFromString() {
		assertEquals(TokenValidationProtocol.contrail, TokenValidationProtocol
				.fromString("CONTRAIL"));
	}
}
