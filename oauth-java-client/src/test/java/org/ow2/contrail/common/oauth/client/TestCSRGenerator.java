package org.ow2.contrail.common.oauth.client;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.util.encoders.Base64;
import org.junit.Test;
import org.ow2.contrail.common.oauth.client.utils.CSRGenerator;

import eu.emi.security.authn.x509.helpers.CertificateHelpers;
import eu.emi.security.authn.x509.helpers.proxy.ProxySAMLExtension;
import eu.emi.security.authn.x509.helpers.proxy.X509v3CertificateBuilder;
import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
import eu.emi.security.authn.x509.impl.CertificateUtils.Encoding;

public class TestCSRGenerator {
	@Test
	public void test() throws Exception {
		CSRGenerator g = new CSRGenerator();

		PKCS10CertificationRequest csr = g.generate("CN=TestUser");

		StringWriter writer = new StringWriter();
		PEMWriter pemWriter = new PEMWriter(writer);
		pemWriter.writeObject(csr);
		pemWriter.flush();
		pemWriter.close();

		String pemEncoded = writer.toString();

		KeyPair kp = g.getKeyPair();

		java.security.interfaces.RSAPublicKey pub = (java.security.interfaces.RSAPublicKey) kp
				.getPublic();
//		System.out.println(pub.getModulus());
		RSAPrivateKey key = (RSAPrivateKey) kp.getPrivate();
//		System.out.println(key.getModulus());
		
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		CertificateUtils.savePrivateKey(os, key, CertificateUtils.Encoding.PEM,
				null, "".toCharArray(), false);
		System.out.println(os.toString("UTF-8"));

		generateShortLivedCertificate(pemEncoded);

	}

	public String generateShortLivedCertificate(String strCsr) throws Exception {
		// CSRGenerator csrGen = new CSRGenerator();
		// PKCS10CertificationRequest csr = csrGen.generate(userDN);
		System.out.println(strCsr);
		// signing and generating the certificate
		KeyAndCertCredential caCred = getCACredential(
				"src/test/resources/ca.crt", "src/test/resources/ca.key",
				"theca");
		// 15 minutes ago
		final long CredentialGoodFromOffset = 1000L * 60L * 15L;

		final long startTime = System.currentTimeMillis()
				- CredentialGoodFromOffset;
		// expiration time of 6 hrs
		final long endTime = startTime + 30 * 3600 * 1000;

		X500Name issuerX500Name = CertificateHelpers.toX500Name(caCred
				.getCertificate().getSubjectX500Principal());
		String issuerName = caCred.getCertificate().getSubjectDN().getName();
		Random rand = new Random();

		// build DN from username

		// X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(
		// issuerX500Name, new BigInteger(20, rand), new Date(startTime),
		// new Date(endTime), csr.getSubject(), csr.getSubjectPublicKeyInfo());
		String uuid = UUID.randomUUID().toString();
		final String subjectName = String.format("CN=%s, CN=%s", uuid,
				"test-user");

		X500Principal subjectDN = new X500Principal(issuerName + ","
				+ subjectName);
		X500Name subjectX500Name = CertificateHelpers.toX500Name(subjectDN);
		
		PKCS10CertificationRequest csr = new PKCS10CertificationRequest(convertRsaPemToDer(strCsr));
		
		X509v3CertificateBuilder certBuilder = new X509v3CertificateBuilder(
				issuerX500Name, new BigInteger(20, rand), new Date(startTime),
				new Date(endTime), subjectX500Name,
				csr.getSubjectPublicKeyInfo());

		AlgorithmIdentifier sigAlgId = X509v3CertificateBuilder
				.extractAlgorithmId(caCred.getCertificate());

		String signatureAlgorithm = "SHA1withRSA";

		// add extension
		GeneralNames subjectAltNames = new GeneralNames(new GeneralName(
				GeneralName.uniformResourceIdentifier, "urn:uuid:" + uuid));
		certBuilder.addExtension(Extension.subjectAlternativeName, false,
				subjectAltNames.toASN1Primitive());

		ProxySAMLExtension extValue = new ProxySAMLExtension(
				"<saml>hello world!</saml>");
		certBuilder.addExtension(new ASN1ObjectIdentifier(
				ProxySAMLExtension.SAML_OID), false, extValue);

		X509Certificate certificate = certBuilder.build(caCred.getKey(),
				sigAlgId, signatureAlgorithm, null, null);

		java.security.interfaces.RSAPublicKey pub = (java.security.interfaces.RSAPublicKey) certificate
				.getPublicKey();
		// System.out.println(pub.getModulus());

		certificate.checkValidity(new Date());
		certificate.verify(caCred.getCertificate().getPublicKey());

		ByteArrayOutputStream os = new ByteArrayOutputStream();

		CertificateUtils.saveCertificate(os, certificate,
				CertificateUtils.Encoding.PEM);
		String strPub = os.toString("UTF-8");
		System.out.println(strPub);
		return strPub;

	}

	private KeyAndCertCredential getCACredential(String caCertPath,
			String caKeyPath, String password) throws Exception {
		InputStream isKey = new FileInputStream(caKeyPath);
		PrivateKey pk = CertificateUtils.loadPrivateKey(isKey, Encoding.PEM,
				password.toCharArray());

		InputStream isCert = new FileInputStream(caCertPath);
		X509Certificate caCert = CertificateUtils.loadCertificate(isCert,
				Encoding.PEM);

		if (isKey != null)
			isKey.close();
		if (isCert != null)
			isCert.close();

		return new KeyAndCertCredential(pk, new X509Certificate[] { caCert });
	}

	public static byte[] convertRsaPemToDer(String pemData) throws IOException {

		// Strip PEM header and footer
		int headerEndOffset = pemData.indexOf('\n');
		int footerStartOffset = pemData.indexOf("-----END");
		String strippedPemData = pemData.substring(headerEndOffset + 1,
				footerStartOffset - 1);

		// Decode Base64 PEM data to DER bytes
		byte[] derBytes = fromBase64(strippedPemData);
		return derBytes;
	}

	public static byte[] fromBase64(String b64Data) {
		byte[] decoded = Base64.decode(b64Data);
		return decoded;
	}
}
