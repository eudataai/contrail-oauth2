package org.ow2.contrail.common.oauth.client;

import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.junit.Test;

import eu.emi.security.authn.x509.X509Credential;
import eu.emi.security.authn.x509.helpers.CertificateHelpers;
import eu.emi.security.authn.x509.helpers.FlexiblePEMReader;
import eu.emi.security.authn.x509.helpers.CertificateHelpers.PEMContentsType;
import eu.emi.security.authn.x509.helpers.proxy.ProxyCertificateImpl;
import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.CertificateUtils.Encoding;
import eu.emi.security.authn.x509.impl.PEMCredential;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class TestProxyCertificate {
	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Test
	public void test() throws Exception {
		FileInputStream fs = new FileInputStream(new File("src/test/resources/rcauth-proxy.pem"));
		// X509Certificate[] certChain =
		// CertificateUtils.loadCertificateChain(fs, Encoding.PEM);
		// System.out.println(CertificateUtils.loadPEMPrivateKey(fs, null));
		// System.out.println(certChain.length);
		// org.eclipse.jetty.util.security.CertificateUtils
		FileReader fr = new FileReader(new File("src/test/resources/rcauth-proxy.pem"));
		PemReader r = new PemReader(fr);
		PemObject p = r.readPemObject();
		System.out.println(loadCertificateChain(p).length);

		// System.out.println(CertificateUtils.loadPrivateKey(fs, Encoding.PEM,
		// null));

		// CertificateFactory cf = CertificateFactory.getInstance("X.509");
		// System.out.println(cf.generateCertificate(fs));
		// System.out.println(getPrivate("src/test/resources/rcauth-proxy.pem"));
		// System.out.println(getPublic("src/test/resources/rcauth-proxy.pem"));
		// X509Credential cred = new
		// PEMCredential("src/test/resources/rcauth-proxy.pem",
		// "".toCharArray());
		// System.out.println(cred.getCertificateChain());

		// X509Credential cred = new
		// PEMCredential("src/test/resources/rcauth-proxy.pem",
		// "".toCharArray());
		// System.out.println(cred);
	}

	@Test
	public void test2() throws Exception {
		PEMParser pemParser = new PEMParser(new FileReader(new File("src/test/resources/rcauth-proxy.pem")));
		Object pubObject = pemParser.readPemObject();
		System.out.println(loadCertificateChain((PemObject) pubObject)[0].getSubjectDN());
		// X509CertificateHolder holder = (X509CertificateHolder) pubObject;
		// JcaPEMKeyConverter converter = new
		// JcaPEMKeyConverter().setProvider("BC");
		// System.out.println (converter.getKeyPair((PEMKeyPair)
		// pemParser.readObject()));
	}

	@Test
	public void test3() throws Exception {
		PEMParser pemParser = null;
		try {
			pemParser = new PEMParser(new FileReader(new File("src/test/resources/rcauth-proxy.pem")));
//			Object pubObject = pemParser.readObject();
//			System.out.println(pubObject);
//			X509CertificateHolder holder = (X509CertificateHolder) pubObject;
			JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
			Object pubObject = pemParser.readObject();
			System.out.println(converter.getKeyPair((PEMKeyPair) pubObject));
					
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			pemParser.close();
		}
	
	}

	public static PrivateKey getPrivate(String filename) throws Exception {

		byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());

		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}

	public static PublicKey getPublic(String filename) throws Exception {

		byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());

		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}

	public static X509Certificate[] loadCertificateChain(PemObject pem) throws IOException {
		InputStream realIS = null;

		boolean readOne = false;
		ByteArrayOutputStream buffer = new ByteArrayOutputStream(4096);
		// do {
		// if (pem == null && readOne == false)
		// throw new IOException("PEM data not found in the stream and its end
		// was reached");
		// if (pem == null)
		// break;
		PEMContentsType type = CertificateHelpers.getPEMType(pem.getType());
		if (!type.equals(PEMContentsType.CERTIFICATE))
			throw new IOException("Expected PEM encoded certificate but found: " + type);
		readOne = true;
		buffer.write(pem.getContent());
		// } while (true);

		realIS = new ByteArrayInputStream(buffer.toByteArray());
		X509Certificate[] unsorted = loadDERCertificateChain(realIS);
		List<X509Certificate> unsortedList = new ArrayList<X509Certificate>();
		Collections.addAll(unsortedList, unsorted);
		return CertificateHelpers.sortChain(unsortedList);
	}

	private static X509Certificate[] loadDERCertificateChain(InputStream is) throws IOException {
		Collection<? extends Certificate> certs = CertificateHelpers.readDERCertificates(is);
		Iterator<? extends Certificate> iterator = certs.iterator();
		X509Certificate[] ret = new X509Certificate[certs.size()];
		for (int i = 0; i < ret.length; i++) {
			Certificate c = iterator.next();
			if (!(c instanceof X509Certificate))
				throw new IOException("The DER input contains a certificate which is not a "
						+ "X.509Certificate, it is " + c.getClass().getName());
			ret[i] = (X509Certificate) c;
		}
		return ret;
	}

}
