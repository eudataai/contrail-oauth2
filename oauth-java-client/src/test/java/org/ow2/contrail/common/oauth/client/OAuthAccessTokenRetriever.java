package org.ow2.contrail.common.oauth.client;

import org.ow2.contrail.common.oauth.rp.TokenValidator;
import org.ow2.contrail.common.oauth.rp.TokenValidatorFactory;

import eu.unicore.security.canl.AuthnAndTrustProperties;
import eu.unicore.security.canl.CredentialProperties;
import eu.unicore.security.canl.TruststoreProperties;
import eu.unicore.security.canl.TrustedIssuersProperties.TruststoreType;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Properties;

public class OAuthAccessTokenRetriever {

    public static void main(String[] args) throws Exception {
    	OAuthAccessTokenRetriever a = new OAuthAccessTokenRetriever();
//		TokenValidator val = TokenValidatorFactory.newInstance(TokenValidationProtocol.unity, "https://localhost:2443/oauth2/tokeninfo", a.getAuthnAndTrustProperties());
//		System.out.println(val.verifyToken("x5h5c6LbI_ax93m9mwuPDXouebdQmXRDEHxTbc3U_8U"));
		
		TokenValidator val = TokenValidatorFactory.newInstance(TokenValidationProtocol.unity, "https://unity.sara.vm.surfsara.nl:2443/oauth2/tokeninfo", a.getCAAuthnAndTrustProperties());
		ITokenInfo tokenInfo = val.verifyToken("JK7QvwkFQ4JO-qSiGpu6aDqUhjc82A4XbH1fVLrCJho");
		System.out.println(val.verifyToken("JK7QvwkFQ4JO-qSiGpu6aDqUhjc82A4XbH1fVLrCJho"));
		System.out.println("cuurent timestamp: "+Calendar.getInstance().getTimeInMillis());
		IUserInfo u = val.getUserInfo(tokenInfo.getAccessToken());
		System.out.println(u);
    }
    
    
    public AuthnAndTrustProperties getAuthnAndTrustProperties() {
		Properties p = new Properties();
		// Credential Properties
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_FORMAT,
				"jks");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_KEY_LOCATION, "src/test/resources/keystore.jks");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_LOCATION, "src/test/resources/keystore.jks");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_KS_KEY_PASSWORD, "storepass");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_PASSWORD, "storepass");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_KS_ALIAS, "unity.sara.vm.surfsara.nl");

		// truststore properties
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_TYPE,
				TruststoreType.keystore.toString());
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_TYPE,
				"jks");
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_PATH,
				"src/test/resources/keystore.jks");
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_KS_PASSWORD,
				"storepass");
			
		AuthnAndTrustProperties auth = new AuthnAndTrustProperties(p);
		
		return auth;
	}
    
    public AuthnAndTrustProperties getCAAuthnAndTrustProperties() {
		Properties p = new Properties();
		// Credential Properties
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_FORMAT,
				"PEM");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_KEY_LOCATION, "src/test/resources/ca.key");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_LOCATION, "src/test/resources/ca.pem");
		p.setProperty(CredentialProperties.DEFAULT_PREFIX
				+ CredentialProperties.PROP_PASSWORD, "eudat");

		// truststore properties
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_TYPE,
				TruststoreProperties.TruststoreType.directory.toString());
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_DIRECTORY_LOCATIONS+"1",
				"src/test/resources/ca.pem");
		p.setProperty(TruststoreProperties.DEFAULT_PREFIX
				+ TruststoreProperties.PROP_DIRECTORY_ENCODING,
				"PEM");
		
		AuthnAndTrustProperties auth = new AuthnAndTrustProperties(p);
		
		return auth;
	}
}
